<?php 

 class Category {
    private $conn;
    private $table = 'categories';


    public $id;
    public $name;
    public $created_at;

    public function __construct($db) {
        $this->conn = $db;
      }

    public function read ()
    {
        $query = 'SELECT
        id,
        name,
        created_at
      FROM
        ' . $this->table . '
      ORDER BY
        created_at DESC';

        $statement = $this->conn->prepare($query);

        $statement->execute();

        return $statement;
    }

    public function read_single()
    {
        $query = 'SELECT 
        id, 
        name,
        created_at FROM ' . $this->table . 
        ' WHERE id = ?
        LIMIT 0,1';

        $statement = $this->conn->prepare($query);
        $statement->bindParam(1, $this->id);
        $statement->execute();

        $row = $statement->fetch(PDO::FETCH_ASSOC);

        $this->id = $row['id'];
        $this->name = $row['name'];
        $this->created_at = $row['created_at'];
    }
 }
?>