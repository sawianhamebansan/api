<?php 

  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With'); //the x-requested list here helps in XSS attack

  include_once '../../config/connect.php';
  include_once '../../models/post.php';


  $database = new Database();
  $db = $database->connect();

  $post = new Post($db); 


  $data = json_decode(file_get_contents("php://input"));

  $post->title = $data->title;
  $post->body = $data->body;
  $post->author = $data->author;
  $post->category_id = $data->category_id;

  if ($post->create())
  {
    echo json_encode(
        array('message' => 'Post Created')
    );
  } else {
    echo json_encode(
        array('message' => 'Post Could Not Be Created')
    );
  }

  ?>