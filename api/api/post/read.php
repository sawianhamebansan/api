<?php 

  header('Access-Control-Allow-Origin: *');
  // header('Access-Control-Allow-Origin: https://0d9b-103-112-83-12.ngrok-free.app');
  header('Referrer-Policy: strict-origin-when-cross-origin');
  header('Content-Type: application/json');

  include_once '../../config/connect.php';
  include_once '../../models/post.php';


  $database = new Database();
  $db = $database->connect();

  $post = new Post($db);

  $result = $post->read();

  $num = $result->rowCount();


  if($num > 0) {

    $posts_arr = array();


    while($row = $result->fetch(PDO::FETCH_ASSOC)) {
      extract($row);

      $post_item = array(
        'id' => $id,
        'title' => $title,
        'body' => html_entity_decode($body),
        'author' => $author,
        'category_id' => $category_id,
        'category_name' => $category_name,
        'created_at' => $created_at
      );


      array_push($posts_arr, $post_item);

    }


    echo json_encode($posts_arr);

  } else {

    echo json_encode(
      array('message' => 'No Posts Found')
    );
  }