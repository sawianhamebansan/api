
let baseURL = 'https://3407-103-112-83-12.ngrok-free.app/';
$(document).ready(function(){






  // viewing the data
    $.ajax({
        url : baseURL + 'Internship/projectMBDA/phpRest/api/read.php',
        headers: {
          'ngrok-skip-browser-warning': true
        },
        type: 'GET',
        success: function(response) {
          let data = JSON.parse(response)
          let html = ''

          data.data.forEach((item,index) => {
            html += `
                   <tr>
                          <td>`+(index + 1)+`</td>
                          <td class="styleid">`+item.id+`</td>
                          <td class ="newauthor">`+ item.author + `</td>
                          <td class = "newbody">`+ item.body + `</td>
                          <td class="edit-col"><button class="edit-btn" data-id="${item.id}")>Edit</button></td>
                          <td class="delete-col"><button class="delete-btn" data-id="${item.id}">Delete</button></td>
                    </tr>
            `      
          });
          $('#dataContainer').html(html);
        },  
    });



    // edit function it get call from the edit btn
    function edit (element, id) {

      
      var entryAuthor = $(element).closest('tr').find('td:nth-child(3)').text();
      var entryBody = $(element).closest('tr').find('td:nth-child(4)').text();
  
      $('#editEntryId').val(id);
      $('#editAuthor').val(entryAuthor.trim());
      $('#editBody').val(entryBody.trim());
  

      var newBody = $('#editBody').val();
      console.log(newBody);
  

      $('#saveChanges').on('click', function() {
      
        var entryId = $('#editEntryId').val();
        var newAuthor = $('#editAuthor').val();
        var newBody = $('#editBody').val();
        var newCategory = $('#editCat').val();
        var newTitle = $('#editTitle').val();

        $.ajax({
          url: baseURL + 'Internship/projectMBDA/phpRest/api/update.php',
          type: 'PUT',
          headers: {
            'ngrok-skip-browser-warning': true
          },
          data: JSON.stringify({
            id: entryId,
            title: newTitle,
            author: newAuthor,
            body: newBody, 
            category_id: newCategory
          }),
          success: function(response) {
            console.log(response);
            console.log('Entry Updated Successfully');

            $('#editPopup').fadeOut();

            var newAuthorElement = $(element).closest('tr').find('.newauthor');
                newAuthorElement.text(newAuthor);

            var newBodyElement = $(element).closest('tr').find('.newbody');
                newBodyElement.text(newBody);

                console.log(newAuthor);


          },
          error: function(xhr, status, error) {
            console.log('Error updating entry:', error);
          }
        });
    });

    }

    function save (element) {
      console.log($(element).parent().text())
    }




    // delete script
    $(document).on('click', '.delete-btn', function() {
      var entryId = $(this).data('id');
  
      // Confirm deletion
      if (confirm("Are you sure you want to delete this entry?")) {
        
        $.ajax({
          url: baseURL + 'Internship/projectMBDA/phpRest/api/delete.php',
          type: 'DELETE',
          headers: {
            'ngrok-skip-browser-warning': true
          },
          data: JSON.stringify({ id: entryId }),
          success: function(response) {
          
          console.log(response);
          
            console.log('Entry deleted successfully.'+ entryId);
            $(entryId).remove();


            location.reload();
          },
          error: function(xhr, status, error) {

            console.error('Error deleting entry:', error);
          }
        });
      }
    });


    // edit button
    $(document).on('click', '.edit-btn', function() {
      var id = $(this).data('id');
      edit(this, id);
      $('#editPopup').fadeIn();
    });
    $(document).on('click', '.save-btn', function() {
      save(this);
    })
    $('#cancelEdit').on('click', function() {
      $('#editPopup').fadeOut();
    })   
    });



    // Inserting the data
    function insert(element){

      var title = $('#title').val();
      var author = $('#author').val();
      var body = $('#body').val();
      var category_id = $('#category').val();


      $.ajax({
    
        url: baseURL + 'Internship/projectMBDA/phpRest/api/create.php',
        type: 'POST',
        headers : {
          'ngrok-skip-browser-warning': true
        },
        data: JSON.stringify({
          title: title,
          body: body,
          author: author,
          category_id: category_id
        }),
      

        success: function(response) {
    
          console.log(response);
          console.log('Entry Updated Successfully');

          $('#title').val('');
          $('#author').val('');
          $('#body').val('');
          $('#category').val('');
      }, 
      error: function(xhr, status, error) {
  
        console.log('Error Inserting:', error);
      }
   
    });
  }

